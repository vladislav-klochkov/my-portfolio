const express = require('express');
const router = express.Router();

const Twitter = require('twitter');
const twitterKeys = require('./../../config/twitter-keys.json');
const twitter = Twitter(twitterKeys);

// const axios = require('axios');
// const API = 'https://jsonplaceholder.typicode.com';

router.get('/', (req, res) => {
  "use strict";
  res.send('API is working!');
});

router.get('/tweets', (req, res) => {
  "use strict";
  console.log(req.query.q);
  twitter.get('search/tweets', {q: req.query.q}, function(error, tweets, response) {
    res.send(tweets);
  });
});

// router.get('/posts', (req, res) => {
//   "use strict";
//   axios.get(`${API}/posts`)
//     .then(posts => {
//       res.status(200).json(posts.data);
//     })
//     .catch(error => {
//       res.status(500).send(error);
//     });
// });

module.exports = router;
