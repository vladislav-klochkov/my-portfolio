import { Component, OnInit } from '@angular/core';
import { routerAnimations } from './router.animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [routerAnimations()]
})

export class AppComponent implements OnInit {
  constructor (private _router: Router) { }

  ngOnInit () {
    this.bindKeyNavigationListener();
  }

  bindKeyNavigationListener () {
    const self = this;
    window.addEventListener('keydown', function (e) {
      if (e.keyCode === 39) {
        if (self._router.url === '/home') {
          self._router.navigateByUrl('/about');
        } else if (self._router.url === '/about') {
          self._router.navigateByUrl('/examples');
        }
      } else if (e.keyCode === 37) {
        if (self._router.url === '/examples') {
          self._router.navigateByUrl('/about');
        } else if (self._router.url === '/about') {
          self._router.navigateByUrl('/home');
        }
      }
    }, false);
  }

  prepareRouterAnimation (outlet) {
    const animation = outlet.activatedRouteData['animation'] || {};
    return animation['value'] || null;
  }
}
