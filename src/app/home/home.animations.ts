import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

export function slidingTextAnimation () {
  return trigger('slidingTextAnimation', [
    state('active', style({
      position: 'relative',
      textAlign: 'center',
      margin: '100px 0 0'
    })),
    state('inactive', style({
      position: 'absolute',
      top: '200px',
      right: '100px'
    })),
    transition('active => inactive', animate('1500ms ease-out')),
    transition('inactive => active', animate('1500ms ease-out'))
  ]);
}

