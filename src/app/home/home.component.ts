import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  private canvas: any = null;
  private ctx: any = null;
  private width: any = null;
  private height: any = null;
  private x: any = null;

  private particles = 40;
  private minRadius = 3;
  private maxRadius = 15;
  private speed = 0.001;

  private Bubbles = [];

  constructor () { }

  ngOnInit () {
    this.initializeBubbles();
  }

  bubble () {
    this.canvas.width = this.width;
    this.canvas.height = this.height;
    for (let i = 0; i < this.Bubbles.length; i++) {
      const b = this.Bubbles[i];

      this.ctx.beginPath();
      this.ctx.arc(b.x, b.y, b.r, 0, 3 * Math.PI);

      b.alpha = .5 * (b.y / this.height);
      b.speed += this.speed;

      this.ctx.strokeStyle = 'rgba(239, 83, 80, .3)';
      this.ctx.stroke();
      this.ctx.fillStyle = 'rgba(229, 115, 115,' + b.alpha + ')';
      this.ctx.fill();
      b.y -= b.speed;
      if (b.y < 0) {
        b.y = this.height;
        b.speed = Math.random() * 5;
      }
    }
  }

  draw() {
    this.bubble();
    window.requestAnimationFrame(function () {
      this.draw();
    }.bind(this));
  }

  resizeCanvas() {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.canvas.width = this.width;
    this.canvas.height = this.height;
    this.draw();
  }

  initializeBubbles () {
    this.canvas = document.getElementById('bubbles-canvas');
    this.ctx = this.canvas.getContext('2d');
    this.width = window.innerWidth;
    this.height = window.innerHeight;
    this.x = this.width / this.particles;

    for (let i = 0; i < this.particles; i++) {
      this.Bubbles.push({
        x: i * this.x,
        y: this.height * Math.random(),
        r: this.minRadius + Math.random() * (this.maxRadius - this.minRadius),
        speed: Math.random()
      });
    }

    this.resizeCanvas();
    window.addEventListener('resize', this.resizeCanvas.apply(this), false);
  }
}
