import { NgModule } from '@angular/core';
import { MatDividerModule, MatToolbarModule } from '@angular/material';

@NgModule({
  imports: [
    MatToolbarModule,
    MatDividerModule
  ],
  exports: [
    MatToolbarModule,
    MatDividerModule
  ]
})
export class MaterialModule { }
