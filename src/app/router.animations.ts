import {
  animate, group, query,
  style,
  transition,
  trigger
} from '@angular/animations';

export function routerAnimations () {
  return trigger('routerAnimations', [
    transition('home => about', slideRight),
    transition('home => examples', slideRight),

    transition('about => examples', slideRight),
    transition('about => home', slideLeft),

    transition('examples => about', slideLeft),
    transition('examples => home', slideLeft)
  ]);
}

const slideLeft = [
  query(':leave', style({ position: 'fixed', width: '100%', transform: 'translate(0%)' }), { optional: true }),
  query(':enter', style({ position: 'fixed', width: '100%', transform: 'translate(-100%)' }), { optional: true }),
  group([
    query(':leave', group([
      animate('1000ms ease-in', style({ transform: 'translate(100%)' })),
    ]), { optional: true }),
    query(':enter', group([
      animate('1000ms ease-in', style({ transform: 'translate(0%)' })),
    ]), { optional: true })
  ])
];

const slideRight = [
  query(':leave', style({ position: 'fixed', width: '100%', transform: 'translate(0%)'}), { optional: true }),
  query(':enter', style({ position: 'fixed', width: '100%', transform: 'translate(100%)'}), { optional: true }),

  group([
    query(':leave', group([
      animate('1000ms ease-in', style({ transform: 'translate(-100%)' })),
    ]), { optional: true }),
    query(':enter', group([
      animate('1000ms ease-in', style({ transform: 'translate(0%)' })),
    ]), { optional: true }),
  ])
];

const slideHomeAbout = [
  query(':leave', style({ position: 'fixed', width: '100%', transform: 'translate(0%)' }), { optional: true }),
  query(':enter', style({ position: 'fixed', width: '100%', transform: 'translate(100%)' }), { optional: true }),
  query(':leave .home-page-heading', style({ position: 'absolute', bottom: '50px', left: '50px' }), { optional: true }),
  query(':enter .about-page-heading', style({}), { optional: true }),

  group([
    query(':leave', group([
      animate('500ms ease-in-out', style({ transform: 'translate(-100%)' })),
    ]), { optional: true }),
    query(':enter', group([
      animate('500ms ease-in-out', style({ transform: 'translate(0%)' })),
    ]), { optional: true }),
    query(':enter .sliding-text', group([
      animate('500ms ease-in-out', style({ color: 'blue' })),
    ]), { optional: true })
  ])
];
