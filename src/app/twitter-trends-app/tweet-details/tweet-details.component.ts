import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tweet-details',
  templateUrl: './tweet-details.component.html',
  styleUrls: ['./tweet-details.component.css']
})
export class TweetDetailsComponent {
  @Input() tweet: any;

  constructor() { }
}
