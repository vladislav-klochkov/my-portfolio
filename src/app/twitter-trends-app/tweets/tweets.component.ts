import { Component } from '@angular/core';
import { TweetsService } from './tweets.service';

@Component({
  selector: 'app-tweets',
  templateUrl: './tweets.component.html',
  styleUrls: ['./tweets.component.css']
})

export class TweetsComponent {
  tweets: any = [];
  selectedTweet: any = null;
  searchRequest: string = null;

  constructor(private tweetsService: TweetsService) { }

  onSubmit (e: any) {
    e.preventDefault();
    this.selectedTweet = null;

    this.tweetsService.searchTweets(this.searchRequest)
      .subscribe(
        tweets => {
          this.tweets = tweets.statuses;
          console.log(tweets);
        },
        error => {
          console.log(error);
        },
        () => {
          console.log('Finished');
        }
      );
  }

  onSelectTweet (tweet: any) {
    this.selectedTweet = tweet;
  }
}
