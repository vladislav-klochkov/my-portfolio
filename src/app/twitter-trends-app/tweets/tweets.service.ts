import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class TweetsService {
  constructor(private http: Http) { }

  searchTweets (request: string) {
    return this.http.get('/api/tweets?q=' + request)
      .map(res => res.json());
  }
}
