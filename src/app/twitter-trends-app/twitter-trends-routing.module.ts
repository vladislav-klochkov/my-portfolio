import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TweetsComponent } from './tweets/tweets.component';
import { TwitterTrendsComponent } from './twitter-trends.component';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';

const routes: Routes = [
  {
    path: 'examples/twitter-trends',
    component: TwitterTrendsComponent,
    children: [
      {
        path: 'tweets',
        component: TweetsComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/examples/twitter-trends/tweets'
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forChild(routes) ],
  exports: [ RouterModule ]
})

export class TwitterTrendsRoutingModule { }
