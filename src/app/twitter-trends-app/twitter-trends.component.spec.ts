import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwitterTrendsAppComponent } from './twitter-trends.component';

describe('TwitterTrendsAppComponent', () => {
  let component: TwitterTrendsAppComponent;
  let fixture: ComponentFixture<TwitterTrendsAppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwitterTrendsAppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwitterTrendsAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
