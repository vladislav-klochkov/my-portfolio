import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { TwitterTrendsComponent } from './twitter-trends.component';
import { TweetsComponent } from './tweets/tweets.component';
import { TweetDetailsComponent } from './tweet-details/tweet-details.component';
import { TweetsService } from './tweets/tweets.service';
import { TwitterTrendsRoutingModule } from './twitter-trends-routing.module';

@NgModule({
  declarations: [
    TwitterTrendsComponent,
    TweetsComponent,
    TweetDetailsComponent
  ],
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    TwitterTrendsRoutingModule
  ],
  providers: [ TweetsService ]
})

export class TwitterTrendsModule { }
